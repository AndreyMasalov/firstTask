'use strict';

var btn = document.getElementsByClassName('btn-nav');
var toggle = document.getElementsByClassName('header__container');

document.addEventListener('DOMContentLoaded', function () {
	for (var i = 0; i < btn.length; i++) {
		btn[0].addEventListener('click', function () {
			toggle[0].classList.toggle('header__container--open');
		});
	}
});