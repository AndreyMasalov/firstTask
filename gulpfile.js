var gulp 	= require('gulp');
var pug 	= require('gulp-pug');
var sass 	= require('gulp-sass');
var browserSync = require('browser-sync').create();
var autoprefixer = require('gulp-autoprefixer');

// browserSync
gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: './dist'
        }
    });
    browserSync.watch('./dist/*.html').on('change', browserSync.reload);
});

// Pug to HTML
gulp.task('pug', function () {
	return gulp.src('./src/pug/*.pug')
	.pipe(pug({
		pretty: true
	}))
	.pipe(gulp.dest('./dist/'));
});

// Scss to CSS
gulp.task('scss', function () {
	return gulp.src('./src/scss/*.scss')
	.pipe(sass())
	.pipe(autoprefixer({
	    browsers: ['last 20 versions'],
	    cascade: false
    }))
	.pipe(gulp.dest('./dist/public/css'))
	.pipe(browserSync.stream());
});

// Watch Pug and Scss
gulp.task('watch', function () {
	gulp.watch('./src/pug/*.pug', gulp.series('pug'));
	gulp.watch('./src/scss/*.scss', gulp.series('scss'));
});

// Default task
gulp.task('default', gulp.series(
	gulp.parallel('pug', 'scss'),
	gulp.parallel('watch', 'browser-sync')
	));