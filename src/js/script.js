'use strict'

let btn = document.getElementsByClassName('btn-nav');
let toggle = document.getElementsByClassName('header__container');

document.addEventListener('DOMContentLoaded', () => {
	for (let i = 0; i < btn.length; i++) {
		btn[0].addEventListener('click', () => {
  			toggle[0].classList.toggle('header__container--open');
  		});
	}
});